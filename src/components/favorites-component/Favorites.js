import React, { useState, useEffect, useCallback} from 'react';
import styled from 'styled-components';
import ProductItem from '../product-item/Product-Item';
import ModalEl from '../modal/UseModalElem';
import FetchGet from '../../api/fetch-get/FetchGet';
import FetchPost from '../../api/fetch-post/FetchPost';

const ProductListComponent = styled.ul`
  display: flex;
  flex-wrap: wrap;
  .item__button {
    font-size: 10px;
    color: rgb(255, 255, 255);
    line-height: 1.8;
    text-transform: uppercase;
    border-radius: 8px;
    background-color: rgb(30, 30, 32);
    padding: 5px 6px;
    cursor: pointer;
  }
`;

function Favorites() {
  const [favoritesGames, setFavoritesGames] = useState([]);
  const [modal, setModal] = useState(false);
  const [card, setCard] = useState([]);
  const clickAddToCard = () => {
    const { id, name, price, urlImg, idProduct, color } = card;
    modal ? setModal(false) : setModal(true);
    FetchPost('buy', id, name, price, urlImg, idProduct, color);
  };

  const clickCancel = () => {
    modal ? setModal(false) : setModal(true);
  };

  const getUrl = useCallback(async () => {
    const cardsServer = await FetchGet(`favorites`);
    setFavoritesGames(cardsServer);
  }, []);

  useEffect(() => {
    getUrl();
  }, [getUrl]);

  const removeCard = (idProduct) => {
    setFavoritesGames(
      favoritesGames.filter(({ id }) => {
        return id !== idProduct;
      })
    );
  };
  return (
    <>
      <ProductListComponent>
        {favoritesGames.map(({ name, price, urlImg, idProduct, color }) => {
          return (
            <ProductItem
              key={idProduct}
              name={name}
              price={price}
              urlImg={urlImg}
              idProduct={idProduct}
              color={color}
              active={modal}
              setActive={setModal}
              favoritesCard={removeCard}
              btnText='Add to card'
              classNameButton='item__button'
              addToCard={() => {
                modal ? setModal(false) : setModal(true);
                setCard({
                  id: idProduct,
                  name: name,
                  price: price,
                  urlImg: urlImg,
                  idProduct: idProduct,
                  color: color,
                });
              }}
            />
          );
        })}
      </ProductListComponent>
      
      <ModalEl
        active={modal}
        setActive={setModal}
        header={`Do you want to add this file?`}
        text={`Are you sure you want to add it?`}
        action={
          <div>
            <button onClick={clickAddToCard} className='button'>
              Add
            </button>
            <button onClick={clickCancel} className='button'>
              Cancel
            </button>
          </div>
        }
        closeButton
      />
    </>
  );
}

export default Favorites;