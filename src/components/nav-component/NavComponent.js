import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { AiOutlineDelete } from 'react-icons/ai';
const NavComponentStyle = styled.nav`
  background-color: rgba(200, 0, 255, 1);
  .nav__list {
    display: flex;
    justify-content: space-around;
    list-style: none;
  }
  .nav__item {
    text-align: center;
    padding: 14px 16px;
  }
  a {
    color: black;
  }
`;


function NavComponent() {
  return (
    <NavComponentStyle className='page__nav'>
      <ul className='nav__list'>
        <li className='nav__item'>
          <Link to='/'>GAMES</Link>
        </li>
        <li className='nav__item'>
          <Link to='/favorites'>FAVORITES</Link>
        </li>
        <li className='nav__item'>
        <Link to='/buy'><AiOutlineDelete/></Link>
        </li>
      </ul>
    </NavComponentStyle>
  );
}

export default NavComponent;