import React from 'react';
import styled from 'styled-components';
import { FaTimes } from 'react-icons/fa';
import { AiFillCloseCircle } from 'react-icons/ai';

const ModalBlock = styled.div`
  position: fixed;
  z-index: 166;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.4);
`;

const ModalContent = styled.div`
  background-color: purple;
  top: 50%;
  transform: translateY(-50%);
  margin: auto;
  width: 25%;
  position: relative;
  color: white;
  h2 {
    background-color: rgb(42, 42, 46);
    padding: 26px 0 21px 29px;
    font-size: 22px;
    text-align: center;
    svg {
      padding: 0;
      margin: 0 0 0 80px;
    }
  }
  p {
    font-size: 15px;
    text-align: center;
    padding: 36px;
  }
  button {
    padding: 17px 30px;
    color:white;
    background: linear-gradient(rgba(68, 0, 255, 0.64), rgba(200, 0, 255, 0)) rgb(255,0,177);
    margin-left:40px;
  }
  div {
    display: flex;
    justify-content: center;
  }
`;

const ModalElem = ({
  header,
  text,
  closeButton = false,
  action,
  active,
  setActive,
}) => {
  return (
    <ModalBlock onClick={() => {setActive(false);}}style={active ? { display: 'block' } : { display: 'none' }}>
      <ModalContent onClick={(event) => {event.stopPropagation();}}>
        <h2>
          {header}
          {closeButton ? (<AiFillCloseCircle onClick={() => {setActive(false);}}></AiFillCloseCircle>) : null}
        </h2>
        <p>{text}</p>
          {action}
      </ModalContent>
    </ModalBlock>
  );
};

export default ModalElem;