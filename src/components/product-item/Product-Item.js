import React from 'react';
import styled from 'styled-components';
import { AiOutlineStar } from 'react-icons/ai';
import PropTypes from 'prop-types';
import useLocalStorage from '../localStorage/UseLocal';
import FetchPost from '../../api/fetch-post/FetchPost';
import DeleteFetch from '../../api/fetch-delete/FetchDelete';
import Button from '../button-component/Button.js';

const ItemComponent = styled.li`
  list-style: none;
  margin: 7px;
  padding: 0;
  position: relative;
  background-color: rgba(0, 0, 0, 0.5);
  .item__header {
    display: flex;
    justify-content: space-between;
  }
  .item__img{
    width:300px;
    height:300px;
    object-fit:fill;
  }
  div {
    padding: 5px 20px 10px;
    h2 {
      font-size: 25px;
      color: rgba(102, 0, 255, 0.57);
      font-weight: bold;
      text-align: left;
      padding-bottom: 10px;
    }
    svg {
      margin-bottom: 10px;
    }
    div {
      padding: 0;
    }
    p {
      font-size: 15px;
      color: white;
      font-weight: bold;
      line-height: 1.385;
      margin-bottom: 10px;
    }
    .item__price {
      font-size: 40px;
      color:white;
      font-weight: bold;
      line-height: 1;
      margin: 5px 0;
      &-container {
        display: flex;
        justify-content: space-between;
      }
    }
    .item__button{
      color:white;
      background: linear-gradient(rgba(68, 0, 255, 0.64), rgba(200, 0, 255, 0)) rgb(255,0,177)
    }
  }
`;

function ProductItem({
  id,
  name,
  price,
  urlImg,
  idProduct,
  color,
  btnText,
  addToCard,
  classNameButton,
  favoritesCard,
}) {
  const [favoritesIcon, setStar] = useLocalStorage(idProduct);

  const changeFavoriteIconColor = async () => {
    if (!favoritesIcon) {
      setStar(true);
      await FetchPost(`favorites`, id, name, price, urlImg, idProduct, color);
    } else {
      setStar(false);
      await DeleteFetch('favorites', idProduct);
      if (favoritesCard) {
        favoritesCard(idProduct);
      }
    }
  };

  return (
    <ItemComponent key={idProduct}>
      <img className='item__img' src={urlImg} alt='logo' />
      <div className='item__header'>
        <h2 className='item__heading'>{name}</h2>
        <AiOutlineStar onClick={changeFavoriteIconColor}fill={favoritesIcon ? 'blue' : 'white'}/>
      </div>
        <div>
        <p className='item__idProduct'>Product ID:
          <span className='item__idProduct-name'> {idProduct}</span>
        </p>
        <p className='item__color'>Color: {color}</p>
        <div className='item__price-container'>
          <p className='item__price'>{price} $</p>
          <Button classNameElem={classNameButton} handlClick={addToCard}btnText={btnText}/>
        </div>
      </div>
    </ItemComponent>
  );
}

ProductItem.defaultProps = {
  name: 'NAME',
  price: 777,
  urlImg: 'url',
  idProduct: 767676767,
  color: 'purple',
};

ProductItem.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  urlImg: PropTypes.string,
  idProduct: PropTypes.string,
  color: PropTypes.string,
};

export default ProductItem;