const cardsFromServer = async (path) => {
    try {
      const response = await fetch(`http://localhost:7000/${path}`);
      const cardsRespons = await response.json();
      return cardsRespons;
    } catch (error) {
      console.log(error);
    }
  };
  
  export default cardsFromServer;